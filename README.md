# Postgrest REST server on a postgres database for 1CoinH

This is a work in progress that uses [postgrest](https://postgrest.org) to create an API interface based upon a database schema.

## Clone
```
git clone git@gitlab.com:1coinh/1coinh-server.git
cd 1coinh-server
```

## Fresh

1. Start all containers:

```bash
docker-compose up -d
```

> Check to see the container names, they are needed when running the next set of commands. We have f.i. discovered that on linux mint, the container names will receive _ underscores in their names whereas on Archlinux, dashes are used. You can do this with the command:

> `docker ps --format "{{.Names}}"`

> And you should see a list like:
> ```
> 1coinh-server-server-1
> 1coinh-server-db-1
> 1coinh-server-swagger-1
> ```
> Take notice of the container name difference!

2. Run the schema script to create an empty schema

```bash
docker exec -it 1coinh-server-db-1 psql -U app_user -d app_db -f /home/scripts/schema.sql
```

3. (Optional) Run the fake_data_generator and insert the data in the database

```bash
python -m pip install -r requirements.txt
python fake_data_generator.py
docker exec -it 1coinh-server-db-1 psql -U app_user -d app_db -f /home/scripts/fake_data.sql
```

You can then open [http://localhost:8080](http://localhost:8080) in a browser to see the API interface

If at any time you alter `schema.sql`, you should restart everything with:

```bash
docker-compose restart
```

***

## Remove all data and restart

4. Make sure you stop all containers

```bash
docker-compose down
```

5. remove the `pgdata` sub directory.

```bash
sudo rm -Rf ./pgdata
```

6. restart at **1.**

***

## Test the API

* Go to http://localhost:8080/#/(rpc)%20login/post_rpc_login and press the "try it out" button.
* Use the email and password of one of the users that you uploaded through the `./scripts/fake_data.sql` file
* Make sure that the response contains a token, copy the token to the clipboard.
* Go to the top of the page and press the green `Authorize` button.
* Enter the text `Bearer` followed by a **space** and then past the token.

Now you can try the other API routes and see if you can insert, delete patch or get `idCards`

> You can also enter the token in the `test.sh` file which currently tries to login and tries to insert a record into the `idCards` table. Once everything in test.sh is correct, you can run it.

***
