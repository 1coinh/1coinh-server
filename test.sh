curl -X 'POST' \
  'http://127.0.0.1:3000/rpc/login' \
  -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -d '{
  "email": "rallen@example.com",
  "pass": "test"
}'

curl -X 'POST' \
  'http://127.0.0.1:3000/idcards' \
  -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIjoiYW5vbiIsImVtYWlsIjoicmFsbGVuQGV4YW1wbGUuY29tIiwiZXhwIjoxNjc0ODMyNjYwfQ.ShlX239y1PeHjTZ8D7wJzsqHVvuf7mnYZwVZLQXleck' \
  -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -d '{
  "idcode": "string",
  "issued": true,
  "issuedate": "2023-01-27",
  "userlanguage": "st",
  "userbirthday": "2023-01-27",
  "userfeatures": "string",
  "usergender": "0",
  "userlength": 0,
  "userlengthunit": "0",
  "userlefteye": 0,
  "userrighteye": 0,
  "userhair": 0,
  "photo": "string"
}'

curl -X 'GET' \
  'http://127.0.0.1:3000/idcards' \
  -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIjoiYW5vbiIsImVtYWlsIjoicmFsbGVuQGV4YW1wbGUuY29tIiwiZXhwIjoxNjc0ODMyNjYwfQ.ShlX239y1PeHjTZ8D7wJzsqHVvuf7mnYZwVZLQXleck' \
  -H 'accept: application/json' \
  -H 'Range-Unit: items'
