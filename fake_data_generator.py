from faker import Faker
fake = Faker()

with open("./scripts/fake_data.sql", "w") as file1:
    # Fake User data
    for _ in range(5):
        file1.write(
            f"INSERT INTO basic_auth.users (email, pass, role) values ("
            f"'{fake.email()}',"
            f"'test','anon'"
            ");\n")
