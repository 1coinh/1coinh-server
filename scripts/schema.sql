-- CLEAN --
SET client_min_messages TO WARNING;
drop schema if exists api cascade;
drop schema if exists basic_auth cascade;
drop role if exists anon;
drop role if exists authenticator;

-- JWT for Postgres --
create extension if not exists pgcrypto;
CREATE OR REPLACE FUNCTION url_encode(data bytea) RETURNS text LANGUAGE sql AS $$
    SELECT translate(encode(data, 'base64'), E'+/=\n', '-_');
$$ IMMUTABLE;


CREATE OR REPLACE FUNCTION url_decode(data text) RETURNS bytea LANGUAGE sql AS $$
WITH t AS (SELECT translate(data, '-_', '+/') AS trans),
     rem AS (SELECT length(t.trans) % 4 AS remainder FROM t) -- compute padding size
    SELECT decode(
        t.trans ||
        CASE WHEN rem.remainder > 0
           THEN repeat('=', (4 - rem.remainder))
           ELSE '' END,
    'base64') FROM t, rem;
$$ IMMUTABLE;


CREATE OR REPLACE FUNCTION algorithm_sign(signables text, secret text, algorithm text)
RETURNS text LANGUAGE sql AS $$
WITH
  alg AS (
    SELECT CASE
      WHEN algorithm = 'HS256' THEN 'sha256'
      WHEN algorithm = 'HS384' THEN 'sha384'
      WHEN algorithm = 'HS512' THEN 'sha512'
      ELSE '' END AS id)  -- hmac throws error
SELECT url_encode(hmac(signables, secret, alg.id)) FROM alg;
$$ IMMUTABLE;


CREATE OR REPLACE FUNCTION sign(payload json, secret text, algorithm text DEFAULT 'HS256')
RETURNS text LANGUAGE sql AS $$
WITH
  header AS (
    SELECT url_encode(convert_to('{"alg":"' || algorithm || '","typ":"JWT"}', 'utf8')) AS data
    ),
  payload AS (
    SELECT url_encode(convert_to(payload::text, 'utf8')) AS data
    ),
  signables AS (
    SELECT header.data || '.' || payload.data AS data FROM header, payload
    )
SELECT
    signables.data || '.' ||
    algorithm_sign(signables.data, secret, algorithm) FROM signables;
$$ IMMUTABLE;


CREATE OR REPLACE FUNCTION verify(token text, secret text, algorithm text DEFAULT 'HS256')
RETURNS table(header json, payload json, valid boolean) LANGUAGE sql AS $$
  SELECT
    convert_from(url_decode(r[1]), 'utf8')::json AS header,
    convert_from(url_decode(r[2]), 'utf8')::json AS payload,
    r[3] = algorithm_sign(r[1] || '.' || r[2], secret, algorithm) AS valid
  FROM regexp_split_to_array(token, '\.') r;
$$ IMMUTABLE;

-- ROLES --
create role anon noinherit;
create role authenticator noinherit;
grant anon to authenticator;

-- AUTHENTICATION MECHANISM ---
-- We put things inside the basic_auth schema to hide
-- them from public view. Certain public procs/views will
-- refer to helpers and tables inside.
create schema basic_auth;

create table 
basic_auth.users (
  email    text primary key check ( email ~* '^.+@.+\..+$' ),
  pass     text not null check (length(pass) < 512),
  role     name not null check (length(role) < 512)
);

create or replace function
basic_auth.check_role_exists() returns trigger as $$
begin
  if not exists (select 1 from pg_roles as r where r.rolname = new.role) then
    raise foreign_key_violation using message =
      'unknown database role: ' || new.role;
    return null;
  end if;
  return new;
end
$$ language plpgsql;

drop trigger if exists ensure_user_role_exists on basic_auth.users;
create constraint trigger ensure_user_role_exists
  after insert or update on basic_auth.users
  for each row
  execute procedure basic_auth.check_role_exists();

create or replace function
basic_auth.encrypt_pass() returns trigger as $$
begin
  if tg_op = 'INSERT' or new.pass <> old.pass then
    new.pass = crypt(new.pass, gen_salt('bf'));
  end if;
  return new;
end
$$ language plpgsql;

drop trigger if exists encrypt_pass on basic_auth.users;
create trigger encrypt_pass
  before insert or update on basic_auth.users
  for each row
  execute procedure basic_auth.encrypt_pass();

create or replace function
basic_auth.user_role(email text, pass text) returns name
  language plpgsql
  as $$
begin
  return (
  select role from basic_auth.users
   where users.email = user_role.email
     and users.pass = crypt(user_role.pass, users.pass)
  );
end;
$$;

-- add type
CREATE TYPE basic_auth.jwt_token AS (
  token text
);

-- REGULAR API SChEMA ---
create schema api;

-- login should be on your exposed schema
create or replace function
api.login(email text, pass text) returns basic_auth.jwt_token as $$
declare
  _role name;
  result basic_auth.jwt_token;
begin
  -- check email and password
  select basic_auth.user_role(email, pass) into _role;
  if _role is null then
    raise invalid_password using message = 'invalid user or password';
  end if;

  select sign(
      row_to_json(r), '7QyEEhVnDFRRkURbSxmn5hEVxD0hyyVO'
    ) as token
    from (
      select _role as role, login.email as email,
         extract(epoch from now())::integer + 60*60 as exp
    ) r
    into result;
  return result;
end;
$$ language plpgsql security definer;


--https://www.youtube.com/watch?v=gCo6JqGMi30&list=PL0eyrZgxdwhyfSPF6sHd7Ibm3R0THoOJd&index=3
--https://www.youtube.com/watch?v=wUkKCMEYj9M&t=0s
--mySql data types https:--www.w3schools.com/mysql/mysql_datatypes.asp
CREATE TYPE api.gender AS ENUM ('0', '1', '2');
CREATE TYPE api.lengthUnit AS ENUM ('0', '1', '2');

CREATE TABLE api.idCards (
   cardId BIGINT primary key generated by default as identity,  --Main id of the record
   userId TEXT references basic_auth.users not null default current_setting('request.jwt.claims', FALSE)::json->>'email', --reference to the ID of the user. Maximum of 33 IDCards (and these records) per person.
   idCode varchar(64),             --This is the hash of the Base64 text of the IDCard. Is null as long the card is not issued
   issued bool NOT NULL,           --True is the user issued his new card
   issueDate date NOT NULL,        --Is the date of the issue. Only one issue per day is allowed (to make people think about being carefull when issuing)
   userLanguage char(2) NOT NULL,   --language when issuing IDCard
   userBirthday date NOT NULL,     --date variable
   userFeatures varchar(35),       --35 characters
   userGender api.gender NOT NULL,     --0,1 or 2
   userLength NUMERIC(6,2) NOT NULL, --123.45
   userLengthUnit api.lengthUnit NOT NULL, --m, cm, ft+inch?
   userLeftEye smallint NOT NULL,    --index in our eye list
   userRightEye smallint NOT NULL,   --index in our eye list
   userHair smallint NOT NULL,       --index in our hair list
   photo text NOT NULL       --about 133,000 characters in the base64 image of the IDCard
);

CREATE TABLE api.persBlockchains (
   pbcId BIGINT primary key generated by default as identity,  --Main id of the record
   pbc text NOT NULL          --string max 4,294,967,295 characters, pbc's that are already part of a newer pbc's will be deleted.
);

CREATE TABLE api.transactions (            --the init string is not recorded in this table. This table is used to see how well the 
   transId BIGINT primary key generated by default as identity,         --Main id of the record
   senderId TEXT references basic_auth.users NOT NULL default current_setting('request.jwt.claims', FALSE)::json->>'email',          --usersID (TABLE=users) of Person that pays (sends) money
   receiverId TEXT references basic_auth.users not null,--usersID (TABLE=users) of Person that receives money
   timeStamp timestamp NOT NULL,     --timestamp of the transaction. I'm not sure if something must be indicated between the parentheses.
   securityCode varchar(64),           --hash of a document or image that is used to "prove" the transaction
   amount NUMERIC NOT NULL,            --Total paid amount
   originator1 TEXT references basic_auth.users,                --usersID (TABLE=users) of originator1
   amount1 NUMERIC,                    --Total amount paid from owned coins of originator1
   originator2 TEXT references basic_auth.users,                --usersID (TABLE=users) of originator2
   amount2 NUMERIC,                    --Total amount paid from owned coins of originator2
   originator3 TEXT references basic_auth.users,                --usersID (TABLE=users) of originator3
   amount3 NUMERIC,                    --Total amount paid from owned coins of originator3
   originator4 TEXT references basic_auth.users,                --usersID (TABLE=users) of originator4
   amount4 NUMERIC,                    --Total amount paid from owned coins of originator4
   originator5 TEXT references basic_auth.users,                --usersID (TABLE=users) of originator5
   amount5 NUMERIC              --Total amount paid from owned coins of originator5
   --The hash (from both sides of the transaction) are not added as they can be fully regenerated based on all registered transactions and can also be found in the persBlockchain Table. Matter of fact: Probably we can build and check the sentinel without having to use this table at all since all transactions are also part of the persBlockchains table. But to make life not to difficult, lets create this table, just to be sure.
);

--The payment procedure also needs a table. Anybody that wants to pay can enter an IDCode of a counterparty that could receive. When this happens then de sender and receiver are put in the payments record. The receiver not only sees that sender wants to send him money, he will also receive the most recent PBC of the sender so he is able to do a proposal. The proposal is placed in the amount and 5 originator/amout1-5 lines. A sender or a receiver can (while the line is still present) start a new transaction, which will lead in the available record to be deleted automatically which also sends a notification to the involved people. This is done by the overruled boolean, which needs to result in the sender and receiver field changed into -1. When both are -1 then the record can be automatically be destroyed. So the following record should be able to do the job.
CREATE TABLE api.payments (
   sender TEXT references basic_auth.users default current_setting('request.jwt.claims', FALSE)::json->>'email',    --usersID (TABLE=users) of sender of money
   receiver TEXT references basic_auth.users,  --usersID (TABLE=users) of receiver of money
   amount NUMERIC NOT NULL,            --Total paid amount
   originator1 TEXT references basic_auth.users,                --usersID (TABLE=users) of originator1
   amount1 NUMERIC,                    --Total amount paid from owned coins of originator1
   originator2 TEXT references basic_auth.users,                --usersID (TABLE=users) of originator2
   amount2 NUMERIC,                    --Total amount paid from owned coins of originator2
   originator3 TEXT references basic_auth.users,                --usersID (TABLE=users) of originator3
   amount3 NUMERIC,                    --Total amount paid from owned coins of originator3
   originator4 TEXT references basic_auth.users,                --usersID (TABLE=users) of originator4
   amount4 NUMERIC,                    --Total amount paid from owned coins of originator4
   originator5 TEXT references basic_auth.users,                --usersID (TABLE=users) of originator5
   amount5 NUMERIC,                    --Total amount paid from owned coins of originator5
   overruled bool
);



--the loaded photo is not stored. There is no point doing that. It might stay in the localstorage (also not necessary)
--I am not sure if all the datatypes make sense, but you'll understand their purpose.
--Do we do a DDos protection and a Captiva test before creating new user? Or even create all users ourselves in the beginning?
grant usage on schema api to anon;
grant execute on function api.login(text,text) to anon;
grant ALL on table api.idCards to anon;

-- ROW LEVEL SECURITY
ALTER TABLE api.idCards ENABLE ROW LEVEL SECURITY;
--ALTER TABLE api.persBlockchains ENABLE ROW LEVEL SECURITY;
--ALTER TABLE api.transactions ENABLE ROW LEVEL SECURITY;
--ALTER TABLE api.payments ENABLE ROW LEVEL SECURITY;


-- create the policy to get the rows for the email that is the result of checking the token
create policy idCards_policy ON api.idCards
   USING (current_setting('request.jwt.claims', FALSE)::json->>'email' = userId);
--create policy persBlockchains_policy ON api.persBlockchains
--create policy transactions_policy ON api.transactions
--create policy payments_policy ON api.payments
